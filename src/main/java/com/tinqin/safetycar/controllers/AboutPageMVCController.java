package com.tinqin.safetycar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutPageMVCController {

    @GetMapping("/terms-conditions")
    public String showTermsConditionsPage() {
        return "terms-conditions";
    }

    @GetMapping("/privacy-policy")
    public String showPrivacyPolicyPage() {
        return "privacy-policy";
    }
}
