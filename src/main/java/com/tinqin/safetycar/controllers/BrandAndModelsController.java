package com.tinqin.safetycar.controllers;

import com.tinqin.safetycar.services.contracts.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Set;

@Controller
public class BrandAndModelsController {

    private final CarModelService carModelService;

    @Autowired
    public BrandAndModelsController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @RequestMapping(value = "/models")
    @ResponseBody
    public Set getRegions(@RequestParam String brand) {
        Map<String, Set<String>> models = carModelService.getAllBrandsAndModelsInMap();
        return models.get(brand);
    }
}
