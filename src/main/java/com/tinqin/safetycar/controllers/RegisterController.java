package com.tinqin.safetycar.controllers;

import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.models.verification.OnRegistrationCompleteEvent;
import com.tinqin.safetycar.models.verification.VerificationToken;
import com.tinqin.safetycar.services.contracts.SecurityUserService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Controller
public class RegisterController {
    public static final String INVALID_TOKEN_MESSAGE = "Your token is invalid. Please request another one. If you are still having troubles please contact your agent";
    public static final String EXPIRED_TOKEN_MESSAGE = "Your token has expired! Please request a new one.";
    private final UserDetailsManager userDetailsManager;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final MessageSource messageSource;
    private final ApplicationEventPublisher eventPublisher;
    private final SecurityUserService securityUserService;

    @Autowired
    public RegisterController(UserDetailsManager userDetailsManager, UserService userService,
                              PasswordEncoder passwordEncoder, ApplicationEventPublisher eventPublisher,
                              @Qualifier("messageSource") MessageSource messages,
                              SecurityUserService securityUserService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.messageSource = messages;
        this.eventPublisher = eventPublisher;
        this.securityUserService = securityUserService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("createUserDto", new CreateUserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute CreateUserDto createUserDto,
                               BindingResult bindingResult, Model model, HttpServletRequest request) {
        String errorMessage;
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUserDto", createUserDto);
            model.addAttribute("error", "username/password must be valid");
            return "register";
        }
        if (userDetailsManager.userExists(createUserDto.getEmail())) {
            model.addAttribute("createUserDto", createUserDto);
            errorMessage = "Username is taken";
            model.addAttribute("error", errorMessage);
            return "register";
        }
        if (!createUserDto.getPassword().equals(createUserDto.getPasswordConfirmation())) {
            model.addAttribute("createUserDto", createUserDto);
            errorMessage = "Make Sure you have entered the same password twice";
            model.addAttribute("error", errorMessage);
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User
                        (createUserDto.getEmail(), passwordEncoder.encode(createUserDto.getPassword()), authorities);
        userDetailsManager.createUser(newUser);

        SecurityUser securityUser = securityUserService.getByUsername(newUser.getUsername());
        securityUser.setEnabled(false);
        securityUserService.update(securityUser);

        String appUrl = request.getContextPath();
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(securityUser, request.getLocale(), appUrl));

        User user = Mapper.createUserDtoToUser(createUserDto);
        userService.create(user);
        return "register-confirmation";
    }

    @GetMapping("/confirm-registration")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {
        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            String message = INVALID_TOKEN_MESSAGE;
            model.addAttribute("tokenMsg", message);
            return "register";
        }
        SecurityUser user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = EXPIRED_TOKEN_MESSAGE;
            model.addAttribute("tokenMsg", messageValue);
            return "register";
        }
        userService.enableRegisteredUser(user, verificationToken);
        return "redirect:/create-policy";
    }

}
