package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.models.CreatePolicyDto;
import com.tinqin.safetycar.services.contracts.CalculationTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/calculations")
public class CalculationTableRestController {

    private final CalculationTableService calculationTableService;

    @Autowired
    public CalculationTableRestController(CalculationTableService calculationTableService) {
        this.calculationTableService = calculationTableService;
    }

    @PostMapping
    public double calculateNetPremium(CreatePolicyDto createPolicyDto) {
        return calculationTableService.calculateNetPremium(createPolicyDto);
    }
}
