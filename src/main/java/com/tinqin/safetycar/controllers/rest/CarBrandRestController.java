package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.services.contracts.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.swagger.web.ClassOrApiAnnotationResourceGrouping;

import java.util.List;


@RestController
@RequestMapping("/api/brands")
public class CarBrandRestController {
    private final CarBrandService carBrandService;

    @Autowired
    public CarBrandRestController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping
    public List<CarBrand> getAll() {
        return carBrandService.getAll();
    }

    @GetMapping("/{id}")
    public CarBrand getById(@PathVariable int id) {
        try {
            return carBrandService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public CarBrand create(@RequestBody CarBrand brand) {
        try {
            carBrandService.create(brand);
            return brand;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public CarBrand update(@RequestBody CarBrand editBrand) {
        try {
            CarBrand brand = carBrandService.getById(editBrand.getId());
            brand.setBrand(editBrand.getBrand());
            carBrandService.update(brand);
            return brand;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            carBrandService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}