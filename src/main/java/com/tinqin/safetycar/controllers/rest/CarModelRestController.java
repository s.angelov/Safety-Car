package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.contracts.CarBrandService;
import com.tinqin.safetycar.services.contracts.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/models")
public class CarModelRestController {
    private final CarModelService carModelService;
    private final CarBrandService carBrandService;

    @Autowired
    public CarModelRestController(CarModelService carModelService,
                                  CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @GetMapping
    public List<CarModel> getAll() {
        return carModelService.getAll();
    }

    @PostMapping
    public CarModel create(@RequestBody CarModelDto carModelDto) {
        try {
            CarBrand brand = carBrandService.getById(carModelDto.getBrandId());
            CarModel model = Mapper.carModelDtoToCarModel(carModelDto);
            model.setBrand(brand);
            carModelService.create(model);
            return model;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping
    public CarModel update(@RequestBody CarModel editCarModel) {
        try {
            CarModel model = carModelService.getById(editCarModel.getId());
            model.setModel(editCarModel.getModel());
            model.setBrand(editCarModel.getBrand());
            carModelService.update(model);
            return model;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CarModel getById(@PathVariable int id) {
        try {
            return carModelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            carModelService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
