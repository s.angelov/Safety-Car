package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/region-cities")
public class CitiesRegionsRestController {
    private final CityService cityService;

    @Autowired
    public CitiesRegionsRestController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    public List<City> getCitiesFromRegion(@RequestParam(value = "regionId") Integer regionId) {
        List<City> cities = new ArrayList<>();
        try {
            cities = cityService.getByRegionId(regionId);
        } catch (NullPointerException e) {

        }
        return cities;
    }

}
