package com.tinqin.safetycar.controllers.rest;

import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.services.contracts.CarModelService;
import com.tinqin.safetycar.services.contracts.PolicyService;
import com.tinqin.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/policies")
public class PolicyRestController {

    private final PolicyService policyService;
    private final UserService userService;
    private final CarModelService carModelService;

    @Autowired
    public PolicyRestController(PolicyService policyService, UserService userService, CarModelService carModelService) {
        this.policyService = policyService;
        this.userService = userService;
        this.carModelService = carModelService;
    }

    @GetMapping
    public List<Policy> getAll() {
        return policyService.getAll();
    }

    @GetMapping("/{userId}")
    public List<Policy> getAll(int userId) {
        return policyService.getAllPoliciesByUserId(userId);
    }

    @GetMapping("/expired/{userId}")
    public List<Policy> getAllExpiredPolicies(@PathVariable int userId) {
        try {
            return policyService.getAllExpiredPoliciesByUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/pending/{userId}")
    public List<Policy> getAllPendingPolicies(@PathVariable int userId) {
        try {
            return policyService.getAllPendingPoliciesByUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/rejected/{userId}")
    public List<Policy> getAllRejectedPolicies(@PathVariable int userId) {
        try {
            return policyService.getAllRejectedPoliciesByUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/accepted/{userId}")
    public List<Policy> getAllAcceptedPolicies(@PathVariable int userId) {
        try {
            return policyService.getAllAcceptedPoliciesByUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterBy/{cityId}")
    public List<Policy> getPoliciesByCity(@PathVariable int cityId) {
        try {
            return policyService.getPoliciesByCity(cityId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Policy create(@Valid @RequestBody CreatePolicyDto createPolicyDto) {
        Policy policy = new Policy();
        Car car = new Car();
        car.setCubicCapacity(createPolicyDto.getCubicCapacity());
        car.setModel(carModelService.getByName(createPolicyDto.getCarModel()));
        policy.setCar(car);
        return policy;
    }

    @PutMapping("/{policyId}")
    public Policy update(@RequestBody Policy policy, @PathVariable int policyId, Principal principal) {
        User user = userService.getByEmail(principal.getName());
        Policy policyToUpdate;
        try {
            policyToUpdate = policyService.getById(policyId);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        policyService.update(policyToUpdate, user);
        return policy;
    }

    @GetMapping("/sortBy/{filter}/{order}")
    public List<Policy> sortBy(@PathVariable String filter,
                               @PathVariable(required = false) String order) {
        List<Policy> listToReturn;

        switch (filter) {
            case "submitDate":
                if (order != null && order.equals("desc")) {
                    listToReturn = policyService.sortBySubmitDate();
                    Collections.reverse(listToReturn);
                    return listToReturn;
                }
                return policyService.sortBySubmitDate();

            case "endDate":
                if (order != null && order.equals("desc")) {
                    listToReturn = policyService.sortByEndDate();
                    Collections.reverse(listToReturn);
                    return listToReturn;
                }
                return policyService.sortByEndDate();
        }
        return getAll();
    }
}
