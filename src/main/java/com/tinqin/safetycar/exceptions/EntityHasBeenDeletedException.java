package com.tinqin.safetycar.exceptions;

public class EntityHasBeenDeletedException extends RuntimeException {
    public EntityHasBeenDeletedException(String message) {
        super(message);
    }
}
