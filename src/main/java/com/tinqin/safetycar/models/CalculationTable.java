package com.tinqin.safetycar.models;


import javax.persistence.*;

@Entity
@Table(name="calculation_table")
public class CalculationTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="cc_min")
    private int minCubicCapacity;

    @Column(name="cc_max")
    private int maxCubicCapacity;

    @Column(name="car_age_min")
    private int minAgeCar;

    @Column(name="car_age_max")
    private int maxAgeCar;

    @Column(name="base_amount")
    private double baseAmount;

    public CalculationTable() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMinCubicCapacity() {
        return minCubicCapacity;
    }

    public void setMinCubicCapacity(int minCubicCapacity) {
        this.minCubicCapacity = minCubicCapacity;
    }

    public int getMaxCubicCapacity() {
        return maxCubicCapacity;
    }

    public void setMaxCubicCapacity(int axCubicCapacity) {
        this.maxCubicCapacity = axCubicCapacity;
    }

    public int getMinAgeCar() {
        return minAgeCar;
    }

    public void setMinAgeCar(int minAgeCar) {
        this.minAgeCar = minAgeCar;
    }

    public int getMaxAgeCar() {
        return maxAgeCar;
    }

    public void setMaxAgeCar(int maxAgeCar) {
        this.maxAgeCar = maxAgeCar;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }
}
