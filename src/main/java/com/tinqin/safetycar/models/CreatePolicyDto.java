package com.tinqin.safetycar.models;


import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;

public class CreatePolicyDto implements Serializable {

    private int id;

    private String carBrand;

    private String carModel;

    @Min(value = 1000, message = "Car first registration date must be over 1000.")
    @Max(value = 2020, message = "Car first registration date must before 2020.")
    private Integer carFirstReg;

    private Integer cubicCapacity;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate driverBirthday;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime submitDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime startTime;

    private int paymentTypeId;

    private double totalAmount;

    private boolean accidents;

    private boolean proPlan;

    private String carRegCertificate;

    public CreatePolicyDto() {

    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public LocalDate getDriverBirthday() {
        return driverBirthday;
    }

    public void setDriverBirthday(LocalDate driverBirthday) {
        this.driverBirthday = driverBirthday;
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getCarFirstReg() {
        return carFirstReg;
    }

    public void setCarFirstReg(Integer carFirstReg) {
        this.carFirstReg = carFirstReg;
    }

    public boolean getAccidents() {
        return accidents;
    }

    public void setAccidents(boolean accidents) {
        this.accidents = accidents;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(LocalDateTime submitDate) {
        this.submitDate = submitDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public int getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(int paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public boolean isAccidents() {
        return accidents;
    }

    public boolean isProPlan() {
        return proPlan;
    }

    public void setProPlan(boolean proPlan) {
        this.proPlan = proPlan;
    }

    public String getCarRegCertificate() {
        return carRegCertificate;
    }

    public void setCarRegCertificate(String carRegCertificate) {
        this.carRegCertificate = carRegCertificate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreatePolicyDto that = (CreatePolicyDto) o;
        return id == that.id &&
                paymentTypeId == that.paymentTypeId &&
                Double.compare(that.totalAmount, totalAmount) == 0 &&
                accidents == that.accidents &&
                Objects.equals(carBrand, that.carBrand) &&
                Objects.equals(carModel, that.carModel) &&
                Objects.equals(carFirstReg, that.carFirstReg) &&
                Objects.equals(cubicCapacity, that.cubicCapacity) &&
                Objects.equals(driverBirthday, that.driverBirthday) &&
                Objects.equals(submitDate, that.submitDate) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(startTime, that.startTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carBrand, carModel, carFirstReg, cubicCapacity, driverBirthday, submitDate, startDate, startTime, paymentTypeId, totalAmount, accidents);
    }
}

