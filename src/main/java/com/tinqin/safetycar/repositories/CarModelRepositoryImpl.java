package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;
import com.tinqin.safetycar.repositories.contracts.CarModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class CarModelRepositoryImpl implements CarModelRepository {

    public static final String CAR_MODEL_NOT_FOUND_ERR_MESS = "Car model with id %d not found.";
    public static final String CAR_MODEL_DOESNT_EXIST_ERR_MESS = "Car Model with name %s doesn't exist";
    private final SessionFactory sessionFactory;

    @Autowired
    public CarModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarModel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel ", CarModel.class);
            return query.list();
        }
    }

    @Override
    public void create(CarModel model) {
        try (Session session = sessionFactory.openSession()) {
            session.save(model);
        }
    }

    @Override
    public void update(CarModel model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(model);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarModel modelToDelete = session.get(CarModel.class, id);
            if (modelToDelete == null) {
                throw new EntityNotFoundException(String.format(CAR_MODEL_NOT_FOUND_ERR_MESS, id));
            }
            session.beginTransaction();
            session.delete(modelToDelete);
            session.getTransaction().commit();
        }

    }

    @Override
    public CarModel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarModel model = session.get(CarModel.class, id);
            if (model == null) {
                throw new EntityNotFoundException(String.format
                        (CAR_MODEL_NOT_FOUND_ERR_MESS, id));
            }
            return model;
        }
    }

    @Override
    public CarModel getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where model = :name", CarModel.class);
            query.setParameter("name", name);
            List<CarModel> list = query.list();
            if (!list.isEmpty()) {
                return list.get(0);
            } else {
                throw new EntityNotFoundException(String.format(CAR_MODEL_DOESNT_EXIST_ERR_MESS, name));
            }
        }
    }

    @Override
    public Set<String> getAllModelsByBrand(int brandId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where brand.id = :brandId", CarModel.class);
            query.setParameter("brandId", brandId);
            return query.list().stream().map(CarModel::getModel).collect(Collectors.toSet());
        }
    }

    @Override
    public boolean existByModel(String model) {
       return getAll().stream().anyMatch(m -> m.getModel().equals(model));
    }
}
