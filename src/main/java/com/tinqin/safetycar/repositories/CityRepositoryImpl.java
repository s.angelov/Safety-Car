package com.tinqin.safetycar.repositories;

import com.tinqin.safetycar.models.City;
import com.tinqin.safetycar.models.PaymentType;
import com.tinqin.safetycar.repositories.contracts.CityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepositoryImpl implements CityRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City ", City.class);
            return query.list();
        }
    }

    @Override
    public List<City> getByRegionId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City where region.id =:id", City.class);
            query.setParameter("id", id);
            return query.list();

        }
    }

    @Override
    public City getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(City.class, id);
        }
    }


}
