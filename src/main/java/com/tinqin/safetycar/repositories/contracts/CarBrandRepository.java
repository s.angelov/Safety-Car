package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandRepository {

 List<CarBrand> getAll();

 void create(CarBrand brand);

 void update(CarBrand brand);

 void delete(int id);

 CarBrand getById(int id);

 boolean existByBrandId(int brandId);

 boolean validateUniqueBrand(String brand);



}
