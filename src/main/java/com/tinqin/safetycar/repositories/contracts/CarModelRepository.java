package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.CarModel;

import java.util.List;
import java.util.Set;

public interface CarModelRepository {

    List<CarModel> getAll();

    void create(CarModel model);

    void update(CarModel model);

    void delete(int id);

    CarModel getById(int id);


    CarModel getByName(String name);

    Set<String> getAllModelsByBrand(int brandId);


    boolean existByModel(String model);
}
