package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository {
    void create(Car car);

    void update(Car car);

    void delete(int id);

    List<Car> getAll();

    Car getById(int id);

    boolean existById(int carId);

    boolean checkUserIsOwner(Car car,User user);


}
