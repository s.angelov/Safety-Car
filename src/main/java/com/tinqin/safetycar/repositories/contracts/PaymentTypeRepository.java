package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.PaymentType;

import java.util.List;

public interface PaymentTypeRepository {
    List<PaymentType> getAll();

    PaymentType getById(int id);
}
