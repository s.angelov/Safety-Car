package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.PolicyStatus;

public interface PolicyStatusRepository {
    PolicyStatus getById(int id);
}
