package com.tinqin.safetycar.repositories.contracts;

import com.tinqin.safetycar.models.VehiclePhoto;

import java.util.List;

public interface VehiclePhotoRepository {
    void create(VehiclePhoto photo);

    List<VehiclePhoto> getAllByCarId(int id);
}
