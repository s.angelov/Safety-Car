package com.tinqin.safetycar.services;


import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.repositories.contracts.CarBrandRepository;
import com.tinqin.safetycar.services.contracts.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarBrandServiceImpl implements CarBrandService {

    public static final String CAR_BRAND_NOT_FOUND = "Car brand with id %d not found";
    public static final String CAR_BRAND_ALREADY_EXIST = "This brand already exist!";
    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrand> getAll() {
        return carBrandRepository.getAll();
    }

    @Override
    public void create(CarBrand brand) {
        validateBrand(brand.getBrand());
        carBrandRepository.create(brand);
    }

    @Override
    public void update(CarBrand brand) {
        carBrandRepository.update(brand);

    }

    @Override
    public void delete(int id) {
        carBrandRepository.delete(id);
    }

    @Override
    public CarBrand getById(int id) {
        if (carBrandRepository.getById(id) == null) {
            throw new EntityNotFoundException(String.format(CAR_BRAND_NOT_FOUND, id));
        }
        return carBrandRepository.getById(id);
    }

    private void validateBrand(String brand) {
        if (carBrandRepository.validateUniqueBrand(brand)) {
            throw new DuplicateEntityException(CAR_BRAND_ALREADY_EXIST);
        }
    }
}
