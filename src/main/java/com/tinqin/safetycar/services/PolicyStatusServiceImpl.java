package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.PolicyStatus;
import com.tinqin.safetycar.repositories.contracts.PolicyStatusRepository;
import com.tinqin.safetycar.services.contracts.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PolicyStatusServiceImpl implements PolicyStatusService {
    private final PolicyStatusRepository policyStatusRepository;

    @Autowired
    public PolicyStatusServiceImpl(PolicyStatusRepository policyStatusRepository) {
        this.policyStatusRepository = policyStatusRepository;
    }

    @Override
    public PolicyStatus getById(int id) {
        return policyStatusRepository.getById(id);
    }
}
