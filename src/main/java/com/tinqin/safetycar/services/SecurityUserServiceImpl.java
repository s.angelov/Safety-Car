package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.repositories.contracts.SecurityUserRepository;
import com.tinqin.safetycar.services.contracts.SecurityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecurityUserServiceImpl implements SecurityUserService {

    private final SecurityUserRepository securityUserRepository;

    @Autowired
    public SecurityUserServiceImpl(SecurityUserRepository securityUserRepository) {
        this.securityUserRepository = securityUserRepository;
    }

    @Override
    public SecurityUser getByUsername(String username) {
        return securityUserRepository.getByUsername(username);
    }

    @Override
    public void update(SecurityUser user) {
        securityUserRepository.update(user);
    }
}
