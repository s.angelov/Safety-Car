package com.tinqin.safetycar.services;

import com.tinqin.safetycar.models.VehiclePhoto;
import com.tinqin.safetycar.repositories.contracts.VehiclePhotoRepository;
import com.tinqin.safetycar.services.contracts.VehiclePhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehiclePhotoServiceImpl implements VehiclePhotoService {

    private final VehiclePhotoRepository vehiclePhotoRepository;

    public VehiclePhotoServiceImpl(VehiclePhotoRepository vehiclePhotoRepository) {
        this.vehiclePhotoRepository = vehiclePhotoRepository;
    }

    @Override
    public void create(VehiclePhoto photo) {
        vehiclePhotoRepository.create(photo);
    }

    @Override
    public List<VehiclePhoto> getAllByCarId(int carId) {
        return vehiclePhotoRepository.getAllByCarId(carId);
    }

}
