package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.CarBrand;


import java.util.List;

public interface CarBrandService {

    List<CarBrand> getAll();

    void create(CarBrand brand);

    void update(CarBrand brand);

    void delete(int id);

    CarBrand getById(int id);
}

