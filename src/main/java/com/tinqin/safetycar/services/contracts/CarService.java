package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.User;

import java.util.List;

public interface CarService {

    List<Car> getAll();

    List<Car> getAllCarsByUser(int userId);

    void create(Car car);

    void update(Car car, User user);

    void delete(int id, User user);

    Car getById(int id);
}
