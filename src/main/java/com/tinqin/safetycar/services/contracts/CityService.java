package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.City;

import java.util.List;

public interface CityService {

    List<City> getAll();

    List<City> getByRegionId(int id);

    City getById(int id);
}
