package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;

import java.util.List;

public interface PolicyService {

    List<Policy> getAll();

    void create(Policy policy, int carId);

    void update(Policy policy, User user);

    void delete(int id, User user);

    Policy getById(int id);

    List<Policy> getAllPoliciesByUserId(int id);

    List<Policy> getAllPendingPoliciesByUser(int userId);

    List<Policy> getAllPendingPolicies();

    List<Policy> getAllRejectedPoliciesByUser(int userId);

    List<Policy> getAllRejectedPolicies();

    List<Policy> getAllExpiredPoliciesByUser(int userId);

    List<Policy> getAllExpiredPolicies();

    List<Policy> getAllAcceptedPoliciesByUser(int userId);

    List<Policy> getAllAcceptedPolicies();

    void deleteAllPoliciesByCarId(int carId, User user);

    List<Policy> getPoliciesByCity(int cityId);

    List<Policy> sortBySubmitDate();

    List<Policy> sortByEndDate();
}
