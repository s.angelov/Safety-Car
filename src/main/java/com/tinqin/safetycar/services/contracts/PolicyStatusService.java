package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.PolicyStatus;

public interface PolicyStatusService {

    PolicyStatus getById(int id);
}
