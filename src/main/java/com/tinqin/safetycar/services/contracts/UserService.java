package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;

import java.util.List;

public interface UserService extends IUserService {

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> getAll();

    List<User> getAllClients();

    User getById(int id);

    User getByEmail(String email);

    void acceptPolicy(int policyId, SecurityUser user);

    void rejectPolicy(int policyId, SecurityUser user);

    void deletePolicy(int policyId, SecurityUser user);

    void deletePolicyWithoutSendingEmail(int policyId, SecurityUser user);

    void deleteUser(int userId, SecurityUser user);

    List<User> getAllUsersByBusiness();

    List<User> getAllPersonalUsers();


}
