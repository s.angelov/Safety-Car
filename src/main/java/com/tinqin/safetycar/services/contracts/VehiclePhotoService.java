package com.tinqin.safetycar.services.contracts;

import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.VehiclePhoto;

import java.util.List;

public interface VehiclePhotoService {

    void create(VehiclePhoto photo);

    List<VehiclePhoto> getAllByCarId(int carId);
}
