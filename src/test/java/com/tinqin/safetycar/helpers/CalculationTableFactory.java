package com.tinqin.safetycar.helpers;

import com.tinqin.safetycar.models.CalculationTable;
import com.tinqin.safetycar.models.CreatePolicyDto;

import java.time.LocalDate;

public class CalculationTableFactory {

    public static CreatePolicyDto createPolicy(){
        CreatePolicyDto createPolicyDto=new CreatePolicyDto();
        createPolicyDto.setCubicCapacity(2000);
        createPolicyDto.setCarFirstReg(2004);
        createPolicyDto.setDriverBirthday(LocalDate.now());
        createPolicyDto.setAccidents(true);
        createPolicyDto.setProPlan(true);
        return createPolicyDto;
    }


    public static CalculationTable createTable(){
        CreatePolicyDto createPolicyDto=new CreatePolicyDto();
        createPolicyDto.setCubicCapacity(2000);
        createPolicyDto.setCarRegCertificate("2002");
        createPolicyDto.setDriverBirthday(LocalDate.now());
        createPolicyDto.setAccidents(true);

        CalculationTable table=new CalculationTable();
        table.setId(1);
        table.setMaxAgeCar(15);
        table.setMinAgeCar(2);
        table.setMinCubicCapacity(999);
        table.setMaxCubicCapacity(6000);
        table.setBaseAmount(950.00);
        return  table;
    }
}
