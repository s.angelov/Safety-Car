package com.tinqin.safetycar.helpers;

import com.tinqin.safetycar.models.Car;
import com.tinqin.safetycar.models.CarBrand;
import com.tinqin.safetycar.models.User;

public class CarBrandFactory {

    public static CarBrand createBrand(){
       CarBrand brand= new CarBrand();
       brand.setId(1);
       brand.setBrand("Volvo");
       return brand;
    }

    public static Car createCar(){
        Car car=new Car();
        User user=new User();
        user.setId(1);
        car.setId(1);
        car.setOwner(user);
        car.setCubicCapacity(2000);
        return car;
    }
}
