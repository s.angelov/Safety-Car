package com.tinqin.safetycar.helpers;

import com.tinqin.safetycar.models.*;
import com.tinqin.safetycar.models.verification.VerificationToken;

import java.util.*;

public class HelperFactory {


    public static User createMockUser(){
        User testUser=new User();
        testUser.setId(1);
        testUser.setEmail("testemail@gmail.com");
        testUser.setDeleted(true);
        testUser.setDeleted(false);
        return testUser;
    }


    public static Policy createMockPolicy(){
        PolicyStatus testPolicyStatus=new PolicyStatus();
        testPolicyStatus.setId(1);
        testPolicyStatus.setStatus("pending");

        User testUser=new User();
        testUser.setId(1);
        testUser.setEmail("testemail@gmail.com");

        Policy testPolicy=new Policy();
        testPolicy.setId(1);
        testPolicy.setStatus(testPolicyStatus);
        testPolicy.setAgent(testUser);
        Car car = new Car();
        car.setOwner(testUser);
        testPolicy.setCar(car);
        return testPolicy;
    }

    public static VerificationToken createMockVerificationToken(){
        Date date=new Date();
        date.setTime(5000000);
        VerificationToken testToken=new VerificationToken();
        testToken.setToken("testToken");
        testToken.setExpiryDate(date);
        return testToken;
    }


    public static SecurityUser createMockSecurityUser(){
        SecurityUser testSecurityUser=new SecurityUser();
        testSecurityUser.setUserName("testsecureuser@gmail.com");
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setAuthority("ROLE_ADMIN");
        authorities.add(authority);
        testSecurityUser.setAuthorities(authorities);
        return testSecurityUser;
    }

    public static SecurityUser createMockSecurityNotAdmin(){
        SecurityUser testSecurityUser=new SecurityUser();
        testSecurityUser.setUserName("testsecureuser@gmail.com");
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setAuthority("ROLE_USER");
        authorities.add(authority);
        testSecurityUser.setAuthorities(authorities);
        return testSecurityUser;
    }

    public static VehiclePhoto createVehiclePhoto(){
        VehiclePhoto testPhoto=new VehiclePhoto();
        testPhoto.setId(1);
        testPhoto.setPhoto("jpg");
        testPhoto.setCarId(2);
        return testPhoto;
    }

    public static PolicyStatus createStatus(){
        PolicyStatus testStatus=new PolicyStatus();
        testStatus.setId(1);
        return  testStatus;
    }

    public static PaymentType createType(){
        PaymentType testType=new PaymentType();
        testType.setId(1);
        return  testType;
    }

    public static City createCity(){
        City testCity=new City();
        Region testRegion=new Region();
        testRegion.setId(2);
        testCity.setId(1);
        testCity.setRegion(testRegion);
        return  testCity;
    }
}
