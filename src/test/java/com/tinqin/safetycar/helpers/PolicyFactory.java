package com.tinqin.safetycar.helpers;

import com.tinqin.safetycar.models.*;

import java.util.HashSet;
import java.util.Set;

public class PolicyFactory {

    public static Policy createPolicy(){
        Set<Authority> authorities=new HashSet<>();
        Authority authority=new Authority();
        authority.setAuthority("ROLE_USER");
        authorities.add(authority);
        City city=new City();
        city.setId(1);
        Address address=new Address();
        address.setCity(city);
        User user=new User();
        user.setId(1);
        user.setAuthorities(authorities);
        user.setAddress(address);
        Car car=new Car();
        car.setId(1);
        car.setOwner(user);
        Policy policy=new Policy();
        policy.setId(1);
        policy.setCar(car);
        policy.getPolicyCreator();
        return  policy;
    }

    public static User createUser(){
        Set<Authority> authorities=new HashSet<>();
        Authority authority=new Authority();
        authority.setAuthority("ROLE_USER");
        authorities.add(authority);
        User testUser=new User();
        testUser.setId(1);
        testUser.setAuthorities(authorities);
        return testUser;
    }

    public static User createAdmin(){
        Set<Authority> authorities=new HashSet<>();
        Authority authority=new Authority();
        authority.setAuthority("ROLE_ADMIN");
        authorities.add(authority);
        User testUser=new User();
        testUser.setId(1);
        testUser.setAuthorities(authorities);
        return testUser;
    }
}
