package com.tinqin.safetycar.service;

import com.tinqin.safetycar.services.EmailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {

    @InjectMocks
    private EmailServiceImpl emailService;

    @Mock
    private JavaMailSender mailSender;

    @Test
    public void sendSimpleMessage_ShouldSendEmailMessage() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarproject@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        //Act
        emailService.sendSimpleMessage("to", "subject", "text");
        //Assert
        Mockito.verify(mailSender,Mockito.times(1)).send(message);
    }
}
