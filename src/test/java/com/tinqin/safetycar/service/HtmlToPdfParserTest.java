package com.tinqin.safetycar.service;

import com.lowagie.text.DocumentException;
import com.tinqin.safetycar.services.HtmlToPdfParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@ExtendWith(MockitoExtension.class)
public class HtmlToPdfParserTest {

    @InjectMocks
    private HtmlToPdfParser parser;

    @Mock
    private OutputStream outputStream;

@Mock
    private ITextRenderer iTextRenderer;

//    @Test
//    public void generatePdfFromHtml_ShouldGeneratePdf() throws IOException, DocumentException {
//        //Arrange
//        OutputStream outputStream = new FileOutputStream("output");
//        ITextRenderer renderer = new ITextRenderer();
//        renderer.setDocument("html");
//        renderer.setDocumentFromString("html");
//        //renderer.layout();
//        renderer.createPDF(outputStream);
//        //outputStream.close();
//        //Act
//        parser.generatePdfFromHtml("html", "output");
//        //Assert
//        Mockito.verify(renderer, Mockito.times(1)).createPDF(outputStream);
//
//
//    }


}
