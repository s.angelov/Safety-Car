package com.tinqin.safetycar.service;

import com.tinqin.safetycar.exceptions.DuplicateEntityException;
import com.tinqin.safetycar.exceptions.EntityHasBeenDeletedException;
import com.tinqin.safetycar.exceptions.EntityNotFoundException;
import com.tinqin.safetycar.exceptions.InvalidOperationException;
import com.tinqin.safetycar.models.Policy;
import com.tinqin.safetycar.models.PolicyStatus;
import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.User;
import com.tinqin.safetycar.models.verification.VerificationToken;
import com.tinqin.safetycar.repositories.contracts.*;
import com.tinqin.safetycar.services.UserServiceImpl;
import com.tinqin.safetycar.services.contracts.CarService;
import com.tinqin.safetycar.services.contracts.EmailService;
import com.tinqin.safetycar.services.contracts.PolicyStatusService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.tinqin.safetycar.helpers.HelperFactory.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {


    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private PolicyRepository mockPolicyRepository;

    @Mock
    private VerificationTokenRepository mockVerificationTokenRepository;

    @Mock
    private SecurityUserRepository mockSecurityUserRepository;

    @Mock
    private EmailService mockEmailService;

    @Mock
    private CarService carService;

    @Mock
    private PolicyStatusService mockPolicyStatusService;

    @Mock
    PolicyStatusRepository mockPolicyStatusRepository;

    private User testUser;
    private List<User> testList;
    private VerificationToken testToken;



    @BeforeEach
    public void setup() {
        testUser = createMockUser();
        testToken=createMockVerificationToken();
        testList = new ArrayList<>();
    }


    @Test
    public void create_ShouldCreate_WhenUserDoesntExist() {
        //Arrange
        Mockito.when(mockUserRepository.filterByEmail(Mockito.anyString())).
                thenReturn(testList);

        //Act
        userService.create(testUser);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).create(testUser);
    }


    @Test
    public void create_ShouldThrow_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.filterByEmail(Mockito.anyString())).
                thenReturn(testList);

        //Act,Assert
        testList.add(testUser);
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(testUser));

        Mockito.verify(mockUserRepository,
                Mockito.times(0)).create(testUser);
    }


    @Test
    public void update_ShouldUpdate_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(testUser);

        //Act
        userService.update(testUser);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void update_ShouldThrow_WhenUserDoesntExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(null);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.update(testUser));

        Mockito.verify(mockUserRepository,
                Mockito.times(0)).update(testUser);
    }


    @Test
    public void delete_ShouldDelete_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(testUser);

        //Act
        userService.delete(testUser.getId());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).delete(testUser.getId());
    }

    @Test
    public void getAll_ShouldReturnCollection() {
        //Arrange
        Mockito.when(mockUserRepository.getAll()).thenReturn(testList);

        //Act
        List<User> actualLst = userService.getAll();

        //Assert
        Assertions.assertEquals(actualLst, testList);
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllClients_ShouldReturnCollectionOfAllClients() {
        //Arrange
        Mockito.when(mockUserRepository.getAll()).thenReturn(testList);

        //Act
        List<User> actualLst = userService.getAllClients();

        //Assert
        Assertions.assertEquals(actualLst, testList);
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_ShouldGetById_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(testUser);

        //Act
        userService.getById(testUser.getId());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getById(testUser.getId());
    }


    @Test
    public void getById_ShouldThrow_WhenUserDoesntExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(null);


        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getById(testUser.getId()));

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getById(testUser.getId());
    }


    @Test
    public void getById_ShouldThrow_WhenUserIsDeleted() {
        //Arrange
        testUser.setDeleted(true);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).
                thenReturn(testUser);

        //Assert
        Assertions.assertThrows(EntityHasBeenDeletedException.class,
                () -> userService.getById(testUser.getId()));

//
    }


    @Test
    public void getByEmail_ShouldGetByEmail_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.getByEmail(Mockito.anyString())).
                thenReturn(testUser);

        //Act
        userService.getByEmail(testUser.getEmail());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByEmail(testUser.getEmail());
    }


    @Test
    public void getByEmail_ShouldThrow_WhenUserIsDeleted() {
        //Arrange
        testUser.setDeleted(true);
        Mockito.when(mockUserRepository.getByEmail(Mockito.anyString())).
                thenReturn(testUser);


        //Act, Assert
        Assertions.assertThrows(EntityHasBeenDeletedException.class,
                () -> userService.getByEmail(testUser.getEmail()));
    }


    @Test
    public void getAllUsersByBusiness_ShouldReturnCollection() {
        //Arrange
        Mockito.when(mockUserRepository.getAllBusinessUsers()).thenReturn(testList);

        //Act
        List<User> actualLst = userService.getAllUsersByBusiness();

        //Assert
        Assertions.assertEquals(actualLst, testList);
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAllBusinessUsers();
    }

    @Test
    public void getAllPersonalUsers_ShouldReturnCollection() {
        //Arrange
        Mockito.when(mockUserRepository.getAllPersonalUsers()).thenReturn(testList);

        //Act
        List<User> actualLst = userService.getAllPersonalUsers();

        //Assert
        Assertions.assertEquals(actualLst, testList);
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAllPersonalUsers();
    }


    @Test
    public void getVerificationToken_ShouldReturnToken() {
        //Arrange
        Mockito.when(mockVerificationTokenRepository.getVerificationToken(Mockito.anyString()))
                .thenReturn(testToken);

        //Act
       VerificationToken actual= userService.getVerificationToken(testToken.getToken());

        //Assert
        Assertions.assertEquals(actual,testToken);

        Mockito.verify(mockVerificationTokenRepository,
                Mockito.times(1)).getVerificationToken(testToken.getToken());
    }

    @Test
    public void acceptPolicy_ShouldAcceptPolicy() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();

        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        Mockito.when(mockUserRepository.getByEmail(testUser.getUserName())).thenReturn(newUser);
        Mockito.doNothing().when(mockEmailService).sendSimpleMessage(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

        //Act
        userService.acceptPolicy(policy.getId(),testUser);
        policy.setAgent(mockUserRepository.getByEmail(testUser.getUserName()));

        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).update(policy);
    }

    @Test
    public void acceptPolicy_ShouldThrowIfUserIsNotAdmin() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser notAdmin = createMockSecurityNotAdmin();

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                ()->userService.acceptPolicy(policy.getId(),notAdmin));
        Mockito.verify(mockPolicyRepository,
                Mockito.times(0)).update(policy);
    }

    @Test
    public void rejectPolicy_ShouldRejectPolicy() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();

        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        Mockito.when(mockUserRepository.getByEmail(testUser.getUserName())).thenReturn(newUser);
        Mockito.doNothing().when(mockEmailService).sendSimpleMessage(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

        //Act
        userService.rejectPolicy(policy.getId(),testUser);
        policy.setAgent(mockUserRepository.getByEmail(testUser.getUserName()));

        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).update(policy);
    }

    @Test
    public void deletePolicy_ShouldDeletePolicyIfStatusIsNotActive() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();

        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        Mockito.when(mockUserRepository.getByEmail(testUser.getUserName())).thenReturn(newUser);
        Mockito.doNothing().when(mockEmailService).sendSimpleMessage(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

        //Act
        userService.deletePolicy(policy.getId(),testUser);
        policy.setAgent(mockUserRepository.getByEmail(testUser.getUserName()));

        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).delete(policy.getId());
    }

    @Test
    public void deletePolicy_ShouldThrowExceptionIfStatusIsAccepted() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();
        PolicyStatus status = new PolicyStatus();
        status.setStatus("accepted");
        policy.setStatus(status);
        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> userService.deletePolicy(policy.getId(),testUser));
    }

    @Test
    public void deletePolicyWithoutSendingEmail_ShouldThrowExceptionIfStatusIsAccepted() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();
        PolicyStatus status = new PolicyStatus();
        status.setStatus("accepted");
        policy.setStatus(status);
        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> userService.deletePolicyWithoutSendingEmail(policy.getId(),testUser));
    }

    @Test
    public void deletePolicyWithoutSendingEmail_ShouldDeletePolicyIfStatusIsNotActive() {
        //Arrange
        Policy policy = createMockPolicy();
        Mockito.when(mockPolicyRepository.getById(policy.getId())).
                thenReturn(policy);
        SecurityUser testUser = createMockSecurityUser();

        User newUser = new User();
        newUser.setEmail(testUser.getUserName());

        Mockito.when(mockUserRepository.getByEmail(testUser.getUserName())).thenReturn(newUser);
        //Act
        userService.deletePolicy(policy.getId(),testUser);
        policy.setAgent(mockUserRepository.getByEmail(testUser.getUserName()));

        //Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(1)).delete(policy.getId());
    }

    @Test
    public void deleteUser_ShouldThrowInvalidOperationExceptionIfActivePoliciesArePresent() {
        //Arrange
        User user = createMockUser();
        List<Policy> testList = new ArrayList<>();
        Mockito.when(mockUserRepository.getById(user.getId())).thenReturn(user);
        Mockito.when(mockPolicyRepository.getAllPoliciesByUserId(user.getId())).thenReturn(testList);
        Policy testPolicy = new Policy();
        PolicyStatus status = new PolicyStatus();
        status.setStatus("accepted");
        testPolicy.setStatus(status);
        testList.add(testPolicy);

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> userService.deleteUser(user.getId(),new SecurityUser()));
    }

    @Test
    public void deleteUser_ShouldDeleteUser() {
        //Arrange
        User user = createMockUser();
        SecurityUser secUser = new SecurityUser();
        List<Policy> testList = new ArrayList<>();
        Mockito.when(mockUserRepository.getById(user.getId())).thenReturn(user);
        Mockito.when(mockPolicyRepository.getAllPoliciesByUserId(user.getId())).thenReturn(testList);
        Mockito.when(mockSecurityUserRepository.getByUsername(user.getEmail())).thenReturn(secUser);
        Policy testPolicy = new Policy();
        PolicyStatus status = new PolicyStatus();
        status.setStatus("pending");
        testPolicy.setStatus(status);
        testList.add(testPolicy);

        //Act
        userService.deleteUser(user.getId(), new SecurityUser());
        //Act, Assert
        Mockito.verify(mockPolicyRepository,
                Mockito.times(2)).getAllPoliciesByUserId(user.getId());
        Mockito.verify(carService,
                Mockito.times(1)).getAllCarsByUser(user.getId());
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).delete(user.getId());
        Mockito.verify(mockSecurityUserRepository,
                Mockito.times(1)).update(secUser);
    }

    @Test
    public void getAllUsersByBusiness_ShouldReturnAllBusinessUsers() {
        //Act
        userService.getAllUsersByBusiness();
        //Act, Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAllBusinessUsers();
    }

    @Test
    public void getAllPersonalUsers_ShouldReturnAllPersonalUsers() {
        //Act
        userService.getAllPersonalUsers();
        //Act, Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAllPersonalUsers();
    }

    @Test
    public void createVerificationToken_ShouldCreateVerificationToken() {
        //Arrange
        SecurityUser secUser = new SecurityUser();
        secUser.setUserName("username");
        String token = "token";
        VerificationToken verToken = new VerificationToken();
        verToken.setUser(secUser);
        verToken.setToken(token);
        //Act
        userService.createVerificationToken(secUser,token);
        // Assert
        Mockito.verify(mockVerificationTokenRepository,
                Mockito.times(1)).saveToken(verToken);
    }

    @Test
    public void getVerificationToken_ShouldReturnVerificationToken() {
        //Arrange
        String token = "token";
        //Act
        userService.getVerificationToken(token);
        // Assert
        Mockito.verify(mockVerificationTokenRepository,
                Mockito.times(1)).getVerificationToken(token);
    }

 }



