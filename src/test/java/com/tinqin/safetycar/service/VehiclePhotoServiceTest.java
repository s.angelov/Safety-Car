package com.tinqin.safetycar.service;

import com.tinqin.safetycar.models.SecurityUser;
import com.tinqin.safetycar.models.VehiclePhoto;
import com.tinqin.safetycar.repositories.contracts.VehiclePhotoRepository;
import com.tinqin.safetycar.services.VehiclePhotoServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.tinqin.safetycar.helpers.HelperFactory.createVehiclePhoto;

@ExtendWith(MockitoExtension.class)
public class VehiclePhotoServiceTest {

    @InjectMocks
    private VehiclePhotoServiceImpl vehiclePhotoService;

    @Mock
    VehiclePhotoRepository mockVehiclePhotoRepository;

    private VehiclePhoto testPhoto;
    private List<VehiclePhoto> testList;

    @BeforeEach
    public void setup(){
        testPhoto=createVehiclePhoto();
        testList=new ArrayList<>();
    }
    @Test
    public void crate_ShouldCreate_VehiclePhoto(){
        //Arrange
         Mockito.doNothing().when(mockVehiclePhotoRepository).create(testPhoto);
        //Act
        vehiclePhotoService.create(testPhoto);

        //Assert
        Mockito.verify(mockVehiclePhotoRepository,
                Mockito.times(1)).create(testPhoto);

    }


    @Test
    public void getAllByCarId_ShouldReturn_Collection() {
        //Arrange
        Mockito.when(mockVehiclePhotoRepository.getAllByCarId(Mockito.anyInt()))
                .thenReturn(testList);

        //Act
        List<VehiclePhoto> actual=vehiclePhotoService.getAllByCarId(testPhoto.getCarId());

        //Assert
        Assertions.assertEquals(actual,testList);

        Mockito.verify(mockVehiclePhotoRepository,
                Mockito.times(1)).getAllByCarId(Mockito.anyInt());
    }

}
